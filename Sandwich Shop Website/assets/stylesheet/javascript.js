// Top Navigation scroll effect ===========================

window.onscroll = function () { scrollFunction() }

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("navBar").style.padding = "10px";
    } else {
        document.getElementById("navBar").style.padding = "50px";
    }
}

// Validating form ==========================================


//Upper casing the form =======================================

function myFunction() {
    var x = document.getElementById("name");
    x.value = x.value.toUpperCase();
}

//Validate empty field ==========================================

function checkForm(form) {
    // validation would fail ikf the input is blank
    if(form.inputField.value == "") {
        alert("Error: Input is empty!");
        form.inputField.focus();
        return false;
        }
        
    //Regular expression to match only alphanumeric characters and spaces
    var re = /^[\w]+$/;
    
    //validation fails if the input doesn't match regular expression
    if(!re.test(form.inputField.value)) {
        alert("Error: Input contains invalid characters!");
        form.inputField.focus();
        return false;
    }
    
    //validation was successful
    return true;
}

//Allow only numbers ============================================

// function myFunction() {
//     var x;


//     var x = document.getElementById("number").value;
//     if (isNaN(x) || x < 1 || x > 10) {
//         alert("Input not valid");
//         // then
//         //     alert("Input OK");
//         // }
//     }
// }

// textUpper = document.getElementById("number").value;
// if (textUpper === string);
//     alert("Must input numbers");
// return false;


// function myFunction() {
//     var textUpper;

//     textUpper = document.getElementById("number").value;
//     if (textUpper === String) {
//         alert("Must input numbers");
//         return false;
//     }
// }


// Upper casing form string ============================

// function myFunction() {
//     var textUppercase ;

//     textUppercase = document.getElementById("catering").innerHTML;
//     document.getElementById("catering").innerHTML.toUpperCase() = textUppercase;
// }


// ==== Contact form date and time ========

function displayDate() {
    document.getElementById("time").innerHTML = Date();
}