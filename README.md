## Portfolio Robert Maingu
The Portfolio has three projects, the catering website, realtor website, and the vacation website. 

To get to the catering file site, the path is the Sandwich Shop Website folder -> assets folder.

To get to the realtor files, the path is the realtorsite folder -> assets folder.

To get to the vacation files, the path is the web190 folder -> vacation folder.

## Catering and Realtor sites
These sites are written in pure HTML and CSS. I used the grid template to create different columns. The columns scales as the display size changes.

## Vacation site
This site takes advantage of the Bootstrap framework. I also added an external CSS file for additional style and give the site a responsive mobile-first practice.