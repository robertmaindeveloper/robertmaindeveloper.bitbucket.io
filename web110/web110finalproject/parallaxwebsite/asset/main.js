// Open and closing the form //

function openForm() {
    document.getElementById("myForm").style.display = "inline-block";
}

function closeForm() {
    document.getElementById("myForm").style.display = "none";
}

window.onclick = function(event) {
    if (!event.target.matches('.navHeader')) {
        document.getElementById("navHide").style.display = "none";
    }
}

//Form validation for empty textarea input

function validateForm() {
                let validation = document.forms["myForm"]["msg"].value;
                if (validation == "") {
                alert("Please enter your message!");
                return false;
                  }
                }