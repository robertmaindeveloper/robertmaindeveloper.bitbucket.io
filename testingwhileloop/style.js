// This code ask user for hours of work and pay rate and uses function to calculate gross pay per week, month, and year

const workDays = 5;
const workMonths = 4;
const workYear = 12;

main();

function main() {
    let hour = getHours();
    let rate = getRate();
    let weekly = hour * rate * workDays;
    let monthly = weekly * workMonths;
    let yearly = monthly * workYear;
    displayResults(weekly, monthly, yearly, rate);
}

function getHours() {
    let hour = window.prompt("Enter hours");
    hour = Number(hour);
    return hour
}

function getRate() {
    let rate = window.prompt("Enter rate");
    rate = Number(rate);
    return rate
}

function displayResults(weekly, monthly, yearly, rate) {
    debugger;
    i = 1;
    while (i<=rate) {
        multiply = rate * i;
        document.getElementById("finalAnswer").innerHTML += multiply;
        i = i + 1;
        return multiply;
    }

    document.getElementById("weekFunction").innerHTML = "Weekly pay is $" + weekly;
    document.getElementById("monthFunction").innerHTML = "Monthly pay is $" + monthly;
    document.getElementById("yearFunction").innerHTML = " Annual pay is $" + yearly;
}