// This 

let ORDER = {
    customer: null,
    pizzas: [],
    total: null
}


window.addEventListener("load", function () {
    let elements = document.getElementsByTagName("input");
    let orderFormInputs = document.getElementById("orderForm").elements;
    let customerFormInputs = document.getElementById("customerForm").elements;

    for (let i = 0; i < orderFormInputs.length; i++) {
        orderFormInputs[i].addEventListener("click", updatePizzaTotal)
        orderFormInputs[i].addEventListener("click", checkButtons)
        orderFormInputs[i].addEventListener("focus", checkButtons)
    }

    for (let i = 0; i < customerFormInputs.length; i++) {
        customerFormInputs[i].addEventListener("input", inputInput)
    }

    for (let i = 0; i < elements.length; i++) {
        elements[i].addEventListener("focus", inputFocus);
        elements[i].addEventListener("input", inputInput);
    }

    document.getElementById("add").addEventListener("click", addPizza);
    document.getElementById("submitOrder").addEventListener("click", submitOrder);

    document.getElementById("phone").focus();
});

function inputFocus() {
    document.activeElement.select();
    displayPrompt();
    checkButtons();
}

function displayPrompt(id = null) {
    const prompts = {
        fName: "Minimum of four letter is required",
        lName: "Minimum of four letter is required",
        address: "Enter your address",
        phone: "Enter a valid phone number (xxx-xxx-xxxx)"
    }

    let elements = document.getElementsByTagName("output");

    if (id == null) {
        id = document.activeElement.id;

        for (let i = 0; i < elements.length; i++) {
            elements[i].innerText = "";
        }
    }

    try {
        document.getElementById(id + "-prompt").innerText = prompts[id];
    }
    catch {
        // ignore when the active element is a button
    }
}

function inputInput() {
    let element = document.activeElement;
    let id = element.id;

    if (element.checkValidity()) {
        element.parentElement.className += "entered special instructions"
    } if (id === "phone") {
        checkPhonePattern()
    }
    else {
        document.getElementById(id + "-prompt").innerText = element.validationMessage;
    }
    checkButtons()
}

function Pizza(size = "small", toppings = "cheese", instructions = null, price = 0.00) {
    this.size = size;
    this.toppings = toppings;
    this.instructions = instructions;
    this.price = price;
}

function Customer(user = null, fName = null, lName = null, phone = null, address = null) {
    this.user = user;
    this.fName = fName;
    this.lName = lName;
    this.phone = phone;
    this.address = address
}

function updatePizzaTotal() {
    var pizzaTotal = 0.00;
    var pizzaSubTotal = 0.00;

    document.getElementById("pizzaTotal").innerText = 0.00;
    document.getElementById("pizzaSubTotal").innerText = 0.00;
    document.getElementById("pizzaTaxes").innerText = 0.00;

    if (document.getElementById("medium").checked == true) {
        pizzaSubTotal = getPizzaPrice(1.00);
        pizzaTotal = pizzaSubTotal + pizzaSubTotal * 0.10;
        displayTotal(pizzaTotal, "pizza");

    }
    else if (document.getElementById("large").checked == true) {
        pizzaSubTotal = getPizzaPrice(1.50);
        pizzaTotal = pizzaSubTotal + pizzaSubTotal * 0.10;
        displayTotal(pizzaTotal, "pizza");
    }
    else {
        pizzaSubTotal = getPizzaPrice(0.50)
        pizzaTotal = pizzaSubTotal + pizzaSubTotal * 0.10;
        displayTotal(pizzaTotal, "pizza");
    }

}

function getPizzaPrice(multiplier) {
    const toppPricing = {
        "size": 5.00,
        "pineapple": 0.25,
        "spinach": 0.50,
        "onion": 1.00,
        "chicken": 1.25,
        "cheese": 1.50,
        "beef": 1.75,
        "mushroom": .50
    };
    let inputs = document.getElementById("orderForm").elements;
    let pizzaSubTotal = 0.00;

    for (let i = 0; i < inputs.length; i++) {
        let elem = inputs[i];
        if (elem.checked == true) {
            pizzaSubTotal += toppPricing[elem.value] * multiplier;
        }
    }
    return pizzaSubTotal;
}

function displayTotal(total, id) {
    let orderTotal = total;
    let orderTaxes = total * 0.10;
    let orderSubTotal = orderTotal - orderTaxes;

    document.getElementById(id + "Total").innerText = `$${orderTotal.toFixed(2)}`;
    document.getElementById(id + "SubTotal").innerText = `$${orderSubTotal.toFixed(2)}`;
    document.getElementById(id + "Taxes").innerText = `$${orderTaxes.toFixed(2)}`;
}

function addPizza() {
    let pizzaTotal = Number(document.getElementById("pizzaTotal").innerText.replace("$", ""));

    let pizza = new Pizza(
        getFeature("size"),
        getFeature("toppings"),
        getFeature("instructions"),
        pizzaTotal
    );

    ORDER.pizzas.push(pizza);
    ORDER.total += pizzaTotal

    displayTotal(ORDER.total, "order");
    showPizzas();
    clearInput();
    document.getElementById("add").disabled = true;
}

function getFeature(name) {
    if (name == "instructions") {
        return document.getElementById("instructions").value
    } else {
        let elems = document.getElementsByName(name);
        let features = [];


        for (let i = 0; i < elems.length; i++) {
            if (elems[i].checked) {
                features.push(elems[i].id)
            }
        }

        if (features.length == 1) {
            return features[0];
        }
        else { return features }
    }
}

function clearInput() {
    let tags = document.getElementsByTagName("input");

    for (let i = 0; i < tags.length; i++) {
        tags[i].checked = false;
    }

    document.getElementById("pizzaTotal").innerText = "";
    document.getElementById("pizzaSubTotal").innerText = "";
    document.getElementById("pizzaTaxes").innerText = "";
    document.getElementById("instructions").value = "";
}

function showPizzas() {
    let shownPizzas = document.getElementById("pizzas");
    for (let i = shownPizzas.children.length; i > 0; i--) {
        shownPizzas.removeChild(shownPizzas.childNodes[i]);
    }

    let pizzas = ORDER["pizzas"];
    for (let i = 0; i < pizzas.length; i++) {
        let pizza = (`<li id="${i}">
            <div class="card">
            <div class="card-body">
                <h3>${pizzas[i].size} Pizza</h3>
                <p>${pizzas[i].toppings}</p>
                <p>"${pizzas[i].instructions}"</p>
                <p id="price${i}">$${pizzas[i].price.toFixed(2)}</p>
                <a href="#" onclick="deleteItem()">Delete Item</a>
            </div>
            </div>
            </li>`);
        document.getElementById("pizzas").innerHTML += pizza;
    }
}

function deleteItem() {
    let elem = document.activeElement;
    let pizzaId = Number(elem.parentElement.parentElement.parentElement.id);
    let pizzaPrice = Number(document.getElementById("price" + pizzaId).innerText.replace("$", ""));
    console.log(pizzaPrice);
    ORDER.pizzas.splice(pizzaId, 1);
    ORDER.total -= pizzaPrice
    showPizzas()
    displayTotal(ORDER.total, "order");
}

function checkPhonePattern() {
    let element = document.getElementById("phone");
    let value = element.value;

    if (value.length > 3 && value.substr(3, 1) != "-") {
        value = value.substr(0, 3) + "-" + value.substr(3);
    }

    if (value.length > 7 && value.substr(7, 1) != "-") {
        value = value.substr(0, 7) + "-" + value.substr(7);
    }

    if (element.value != value) {
        element.value = value;
    }
}

function checkButtons() {

    if (document.activeElement.parentElement.parentElement.id == "orderForm") {
        var formName = "orderForm";
        var buttonName = "add"
    }
    else {
        var formName = "customerForm";
        var buttonName = "submitOrder";
    }
    let tags = document.getElementById(formName).elements;

    for (let i = 0; i < tags.length; i++) {
        if (!tags[i].checkValidity()) {
            document.getElementById(buttonName).disabled = true;
            return
        }
    }
    document.getElementById(buttonName).disabled = false;
}

function submitOrder() {
    let url = "https://jsonplaceholder.typicode.com/users";

    let customer = new Customer(
        getFeature("pickupDelivery"),
        document.getElementById("fName").value,
        document.getElementById("lName").value,
        document.getElementById("phone").value,
        document.getElementById("address").value,
    )

    ORDER.customer = customer;

    let data = JSON.stringify(ORDER, null);
    let request = new XMLHttpRequest();
    request.open("POST", url);
    request.setRequestHeader("Content-Type", "application/json");
    request.onreadystatechange = () => {
        console.log(request.status)
        document.getElementById("response").innerText = request.responseText;
    }

    try { request.send(data); }
    catch{ document.getElementById("response").innerText = ORDER }
}